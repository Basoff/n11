#include <iostream>
#include <cmath>
using namespace std;
int main() 
{
	int terra[6][6];
	int path[6][6];
	int move[16];
	int i,j,fuel,posx,posy,way,lsum,sum, lmax, mx, my;
	
	posx=0;
	posy=5;
	fuel=16;
	sum=0;
	lsum=0;	
	
	{
		terra[0][0]=0;
		terra[0][1]=3;
		terra[0][2]=7;
		terra[0][3]=0;
		terra[0][4]=0;
		terra[0][5]=0;
		
		terra[1][0]=10;
		terra[1][1]=0;
		terra[1][2]=0;
		terra[1][3]=0;
		terra[1][4]=6;
		terra[1][5]=0;
		
		terra[2][0]=0;
		terra[2][1]=4;
		terra[2][2]=0;
		terra[2][3]=3;
		terra[2][4]=0;
		terra[2][5]=3;
		
		terra[3][0]=1;
		terra[3][1]=0;
		terra[3][2]=4;
		terra[3][3]=0;
		terra[3][4]=2;
		terra[3][5]=0;
		
		terra[4][0]=0;
		terra[4][1]=1;
		terra[4][2]=0;
		terra[4][3]=0;
		terra[4][4]=0;
		terra[4][5]=9;
		
		terra[5][0]=0;
		terra[5][1]=0;
		terra[5][2]=0;
		terra[5][3]=5;
		terra[5][4]=0;
		terra[5][5]=6;
	}
	
	for(i=0; i<6; i++)
	{
		for(j=0; j<6; j++)
		{
			cout<<terra[j][i]<<"	";
			path[j][i]=1;
		}
		cout<<endl;
	}
	
	while(fuel>0)
	{
		way=(5-posx+posy);
		lmax=0;
		if (way==0)
		{
			break;
		}
		
		if (terra[posx][posy-1]>lmax)
		{
			lmax=terra[posx][posy-1];
			mx=0;
			my=-1;
		}
		if (terra[posx+1][posy-1]>lmax)
		{
			lmax=terra[posx+1][posy-1];
			mx=1;
			my=-1;
		}
		if (terra[posx+1][posy]>lmax)
		{
			lmax=terra[posx+1][posy];
			mx=1;
			my=0;
		}
		if (terra[posx+1][posy+1]>lmax)
		{
			lmax=terra[posx+1][posy+1];
			mx=1;
			my=1;
		}
		if (terra[posx][posy+1]>lmax)
		{
			lmax=terra[posx][posy+1];
			mx=0;
			my=+1;
		}
		if (terra[posx-1][posy+1]>lmax)
		{
			lmax=terra[posx-1][posy+1];
			mx=-1;
			my=1;
		}
		if (terra[posx-1][posy]>lmax)
		{
			lmax=terra[posx-1][posy];
			mx=-1;
			my=0;
		}
		if (terra[posx-1][posy-1]>lmax)
		{
			lmax=terra[posx-1][posy-1];
			mx=-1;
			my=-1;
		}
		
		if(my==-1 & posy>0 & path[posx][posy-1]!=0)
		{
			path[posx][posy]=0;
			posy--;
			cout<<"up"<<endl;
			move[16-fuel]=1;
			fuel--;	
		}
		else if(my==1 & posy<5 & path[posx][posy+1]!=0 & way<fuel)
		{
			path[posx][posy]=0;
			posy++;
			cout<<"down"<<endl;
			move[16-fuel]=8;
			fuel--;	
		}
		else if(mx==-1 & posx>0 & path[posx-1][posy]!=0 & way<fuel)
		{
			path[posx][posy]=0;
			posx--;
			cout<<"left"<<endl;
			move[16-fuel]=3;
			fuel--;	
		}
		else if(mx==1 & posx<5 & path[posx+1][posy]!=0)
		{
			path[posx][posy]=0;
			posx++;
			cout<<"right"<<endl;
			move[16-fuel]=14;
			fuel--;	
		}
		else if(path[posx-1][posy]!=0 & way<fuel-1 & posx>0)
		{
			path[posx][posy]=0;
			posx--;
			cout<<"left"<<endl;
			move[16-fuel]=3;
			fuel--;
		}
		else if(path[posx][posy-1]!=0 & posy>0)
		{
			path[posx][posy]=0;
			posy--;
			cout<<"up"<<endl;
			move[16-fuel]=1;
			fuel--;
		}
		else if(path[posx+1][posy]!=0 & posx<5)
		{
			path[posx][posy]=0;
			posx++;
			cout<<"right"<<endl;
			move[16-fuel]=14;
			fuel--;
		}
		else if(path[posx][posy+1]!=0 & way<fuel-3 & posy<5)
		{
			path[posx][posy]=0;
			posy++;
			cout<<"down"<<endl;
			move[16-fuel]=8;
			fuel--;
		}
		sum+=terra[posx][posy]; cout<<16-fuel<<":"<<sum<<endl;
		terra [posx][posy]=0;
		cout<<endl;
	}
	for (i=1; i<16; i++)
	{
		if (move[i]==1 & move[i-1]==3 | move[i]==3 & move[i-1]==8 | move[i]==8 & move[i-1]==14 | move[i]==14 & move[i-1]==1)
		{
			cout<<"90"<<" ";
		}
		else if (move[i]==1 & move[i-1]==14 | move[i]==14 & move[i-1]==8 | move[i]==8 & move[i-1]==3 | move[i]==3 & move[i-1]==1)
		{
			cout<<"-90"<<" ";
		}
		else
		{
			cout<<0<<" ";
		}
		
	}
	
	cout<<endl<<"Number of possible buildings: "<<sum;
	
	return 0;
}
